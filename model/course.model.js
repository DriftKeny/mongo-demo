const mongoose = require('mongoose');
 
//Attributes of the product object
var productSchema = new mongoose.Schema({
productName: {
type: String,
required: 'this field is required!'
},
productId: {
type: String,
required:'this field is required'
},
author: {
type: String,
required:'this field is required',
min:10,
max: 100
},
productDuration: {
type: String
},
productFee: {
type: String
},
discount: {
type: Number
}
});
 
mongoose.model('product', productSchema);